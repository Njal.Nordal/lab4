package no.uib.inf101.gridview;

import javax.swing.JFrame;
import java.awt.Color;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;

public class Main {
  public static void main(String[] args) {
    
    ColorGrid myGrid = new ColorGrid(3, 4);
    
    myGrid.set(new CellPosition(0, 0), Color.RED);
    myGrid.set(new CellPosition(0, 3), Color.BLUE);
    myGrid.set(new CellPosition(2, 0), Color.YELLOW);
    myGrid.set(new CellPosition(2, 3), Color.GREEN);
    
    GridView gridView = new GridView(myGrid);
    JFrame frame = new JFrame();

    frame.setContentPane(gridView);
    frame.setTitle("INF101");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

    
    
  }
}
