package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import no.uib.inf101.colorgrid.CellColorCollection;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel{
  // TODO: Implement this class

  private IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid colorGrid) {
    setPreferredSize(new Dimension(400, 300));
    this.colorGrid = colorGrid;
  }
  
  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }

  private void drawGrid(Graphics2D g2) {
    double margin = OUTERMARGIN;
    double x = margin;
    double y = margin;
    double width = getWidth() - 2 * margin;
    double height = getHeight() - 2 * margin;
    Rectangle2D frame = new Rectangle2D.Double(x, y, width, height);

    g2.setColor(MARGINCOLOR);
    g2.fill(frame);
    
    CellPositionToPixelConverter cptpc = new CellPositionToPixelConverter(frame, colorGrid, OUTERMARGIN);
    
    drawCells(g2, this.colorGrid, cptpc);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection cellColor, CellPositionToPixelConverter converter) {
    cellColor.getCells().forEach(cell -> {
      Rectangle2D bounds = converter.getBoundsForCell(cell.cellPosition());
      g2.setColor(cell.color() != null ? cell.color() : Color.DARK_GRAY);
      g2.fill(bounds);
    });
  }
}
