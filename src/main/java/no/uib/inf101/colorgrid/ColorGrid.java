package no.uib.inf101.colorgrid;

import java.util.ArrayList;
import java.util.List;
import java.awt.Color;

public class ColorGrid implements IColorGrid{
  // TODO: Implement this class

  private int rows;
  private int cols;
  private ArrayList<ArrayList<Color>> grid;

  public ColorGrid(int rows, int cols) {
  
    this.rows = rows;
    this.cols = cols;
    grid = new ArrayList<>();

    for (int i = 0; i < rows; i++) {
      ArrayList<Color> row = new ArrayList<>(cols);
      for (int j = 0; j < cols; j++) {
        row.add(null);
      }

      grid.add(row);
    }
}

  @Override
  public int rows() {
    return this.rows;
  }
  @Override
  public int cols() {
    return this.cols;
  }
  
  @Override
  public List<CellColor> getCells() {
    ArrayList<CellColor> cellColors = new ArrayList<>();

    for (int row = 0; row < rows; row++) {
      for (int col = 0; col < cols; col++) {
        cellColors.add(new CellColor(new CellPosition(row, col), grid.get(row).get(col)));
      }
    }
    return cellColors;
  }

  @Override
  public Color get(CellPosition pos) {
    return grid.get(pos.row()).get(pos.col());
  }

  @Override
  public void set(CellPosition pos, Color color) {
    grid.get(pos.row()).set(pos.col(), color);
  }

}